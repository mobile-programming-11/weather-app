import 'package:flutter/material.dart';

class WeatherApp extends StatelessWidget {
  const WeatherApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: buildAppBarWidget(),
      body: buildBodyWidget(),
    );
  }

  AppBar buildAppBarWidget() {
    return AppBar(
      title: Text(
        'กาญจนบุรี',
        style: TextStyle(fontSize: 20),
      ),
      backgroundColor: Colors.transparent,
      elevation: 0,
      centerTitle: true,
      leading: IconButton(
        icon: Icon(
          Icons.add,
          size: 30,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      actions: [
        IconButton(
          onPressed: () {},
          icon: Icon(Icons.keyboard_control_outlined),
        )
      ],
    );
  }

  Container buildBodyWidget() {
    return Container(
      child: Stack(children: [
        Image.network(
          "https://images.unsplash.com/photo-1599148400620-8e1ff0bf28d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8OXx8fGVufDB8fHx8&w=1000&q=80",
          fit: BoxFit.cover,
          height: double.infinity,
          width: double.infinity,
        ),
        Container(
          padding: EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 170,
                        ),
                        Text(
                          '24  ํ',
                          style: TextStyle(
                              fontSize: 80,
                              fontWeight: FontWeight.bold,
                              color: Colors.white),
                        ),
                        Text(
                          'มีเมฆ',
                          style: TextStyle(
                            fontSize: 22,
                            color: Colors.white,
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
              weatherYesterday(),
              weatherToday(),
              weatherTomorrow(),
              Column(
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 20),
                    decoration:
                        BoxDecoration(border: Border.all(color: Colors.white)),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 10, bottom: 8),
                    child: weatherActionItems(),
                  )
                ],
              )
            ],
          ),
        )
      ]),
    );
  }
}

Widget weatherYesterday() {
  return ListTile(
      leading: Icon(Icons.wb_twighlight),
      iconColor: Colors.white,
      title: Text(
        "เมื่อวานนี้  ํ แจ่มใส",
        style: TextStyle(
          fontSize: 18,
        ),
      ),
      textColor: Colors.white,
      trailing: Text(
        '30  ํ / 20  ํ',
        style: TextStyle(
          fontSize: 18,
        ),
      ));
}

Widget weatherToday() {
  return ListTile(
      leading: Icon(Icons.cloud),
      iconColor: Colors.white,
      title: Text(
        "วันนี้  ํ มีเมฆ",
        style: TextStyle(
          fontSize: 18,
        ),
      ),
      textColor: Colors.white,
      trailing: Text(
        '29  ํ / 21  ํ ',
        style: TextStyle(
          fontSize: 18,
        ),
      ));
}

Widget weatherTomorrow() {
  return ListTile(
      leading: Icon(Icons.cloud),
      iconColor: Colors.white,
      title: Text(
        "พรุ่งนี้  ํ มีเมฆ",
        style: TextStyle(
          fontSize: 18,
        ),
      ),
      textColor: Colors.white,
      trailing: Text(
        '29  ํ / 21  ํ ',
        style: TextStyle(
          fontSize: 18,
        ),
      ));
}

Widget weatherActionItems() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildWeatherNow(),
      buildWeatherAM(),
      buildWeatherMinday(),
      buildWeatherPM(),
      buildWeatherMidNight(),
    ],
  );
}

Widget buildWeatherNow() {
  return Column(
    children: <Widget>[
      Text(
        'Now',
        style: TextStyle(
            fontSize: 15, fontWeight: FontWeight.bold, color: Colors.white),
      ),
      Text(
        '22   ํ',
        style: TextStyle(
          fontSize: 15,
          color: Colors.white,
        ),
      ),
      Icon(
        Icons.bedtime_rounded,
        color: Colors.white,
      ),
    ],
  );
}

Widget buildWeatherAM() {
  return Column(
    children: <Widget>[
      Text(
        '05:00',
        style: TextStyle(
            fontSize: 15, fontWeight: FontWeight.bold, color: Colors.white),
      ),
      Text(
        '21   ํ ',
        style: TextStyle(
          fontSize: 15,
          color: Colors.white,
        ),
      ),
      Icon(
        Icons.cloudy_snowing,
        color: Colors.white,
      ),
    ],
  );
}

Widget buildWeatherMinday() {
  return Column(
    children: <Widget>[
      Text(
        '12:00',
        style: TextStyle(
            fontSize: 15, fontWeight: FontWeight.bold, color: Colors.white),
      ),
      Text(
        '26   ํ ',
        style: TextStyle(
          fontSize: 15,
          color: Colors.white,
        ),
      ),
      Icon(
        Icons.wb_sunny,
        color: Colors.white,
      ),
    ],
  );
}

Widget buildWeatherPM() {
  return Column(
    children: <Widget>[
      Text(
        '18:00',
        style: TextStyle(
            fontSize: 15, fontWeight: FontWeight.bold, color: Colors.white),
      ),
      Text(
        '27   ํ ',
        style: TextStyle(
          fontSize: 15,
          color: Colors.white,
        ),
      ),
      Icon(
        Icons.light_mode_rounded,
        color: Colors.white,
      ),
    ],
  );
}

Widget buildWeatherMidNight() {
  return Column(
    children: <Widget>[
      Text(
        '00:00',
        style: TextStyle(
            fontSize: 15, fontWeight: FontWeight.bold, color: Colors.white),
      ),
      Text(
        '24   ํ',
        style: TextStyle(
          fontSize: 15,
          color: Colors.white,
        ),
      ),
      Icon(
        Icons.nights_stay,
        color: Colors.white,
      ),
    ],
  );
}
